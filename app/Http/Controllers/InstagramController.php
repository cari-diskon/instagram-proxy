<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;

use Illuminate\Http\Request;

use Phpfastcache\Helper\Psr16Adapter;

use Carbon\Carbon;


require_once('../vendor/autoload.php');


class InstagramController extends Controller
{
    
    public function helloWorld()
    {
        return 'Hello World';
    }

    /**
     * Get Info by Username
     */
    public function getInfoByUsername(Request $request, $type)
    {
        $username = $request->input('username');
        $payload = [];
        $instagram = $this->loginToInstagram();
        $account = $this->getAccountByUsername($username, $instagram);
        $payload['account'] = $this->getAccountArray($account);
        
        if ($type == 'account')
        {
            return json_encode($payload, JSON_UNESCAPED_SLASHES);
        } else if ($type == 'medias')
        {
            $mediasCount = $request->input('count');
            $medias = $this->getMediasByUser($account, $mediasCount, $instagram);
            $payload['medias'] = $this->getMediasArray($medias, $this->getAccountArray($account));
            return json_encode($payload, JSON_UNESCAPED_SLASHES);
        } else {
            return 'yey';
        }
    }

    public function loginToInstagram()
    {
        $instagram = \InstagramScraper\Instagram::withCredentials(env('IG_USERNAME'), env('IG_PASSWORD'), new Psr16Adapter('Files'));
        $instagram->login();
        $instagram->saveSession();
        return $instagram;
    }

    public function getAccountByUsername($username, $instagram)
    {
        $account = $instagram->getAccount($username);

        return $account;
    }

    public function getMediasByUser($account, $count, $instagram)
    {
        $medias = $instagram->getMedias($account->getUsername(), $count);

        return $medias;
    }

    public function getAccountArray($account)
    {
        $dataAccount = [
            'id'=>$account->getId(),
            'username'=>$account->getUsername(),
            'fullName'=>$account->getFullName(),
            'bio'=>$account->getBiography(),
            'profilePictureUrl'=>$account->getProfilePicUrl(),
            'followersCount'=>$account->getFollowsCount()
        ];
        return $dataAccount;
    }

    public function getMediasArray($medias, $accountArray) {
        $dataMedia = [];
        $mediatoday = [];
        $mediayesterday = [];
        $mediaMoreThan2DaysAgo = [];

        $now = time();
        
        foreach ($medias as $media) {
            # code...
            $mediaObject = [
                'id'=>$media->getId(),
                'caption'=>$media->getCaption(),
                'commentsCount'=>$media->getCommentsCount(),
                'createdTimeUnix'=>$media->getCreatedTime(),
                'likesCount'=>$media->getLikesCount(),
                'link'=> $media->getLink(),
                'highResImageUrl'=>$media->getImageHighResolutionUrl(),
                'mediaType'=>$media->getType(),
                'createdTimeCarbon'=>Carbon::createFromTimeStamp($media->getCreatedTime(), 'Asia/Jakarta')->toDateTimeString(),
                'account'=>$accountArray
            ];

            $createdTimeCarbon = Carbon::createFromTimeStamp($media->getCreatedTime(), 'Asia/Jakarta');
            if ($createdTimeCarbon->isCurrentDay())
            {
                /**
                 * today 
                */
                $mediatoday[] = $mediaObject;

            } else if ($createdTimeCarbon->isYesterday())
            {
                /** 
                 * yesterday 
                */
                $mediayesterday[] = $mediaObject;
            } else {
                /**
                 * dua hari lalu
                 */
                $mediaMoreThan2DaysAgo[] = $mediaObject;
            }
        }

        if (!empty($mediatoday))
        {
            $dataMedia['today'] = $mediatoday;
        }

        if (!empty($mediayesterday))
        {
            $dataMedia['yesterday'] = $mediayesterday;
        }

        if (!empty($mediaMoreThan2DaysAgo))
        {
            $dataMedia['moreThan2DaysAgo'] = $mediaMoreThan2DaysAgo;
        }

        return $dataMedia;
    }

}
